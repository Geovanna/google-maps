var geocoder;
var map;
var marker;
 

 //Inicia o mapa usando a localizacao do usuario
function initializeMap() {
	if (navigator.geolocation) {
   navigator.geolocation.getCurrentPosition(function (position)
      { var ponto = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      map.setCenter(ponto);
      map.setZoom(13);

   });
}else{ //se o usuario nao autorizar a localizacao ele ira para essa cordenada
    var latlng = new google.maps.LatLng(-18.8800397, -47.05878999999999);}
    var options = {
        zoom: 5,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
	    scrollwheel: false,
	    disableDoubleClickZoom: true, // <---
	    panControl: false,
	    streetViewControl: false
    };

    map = new google.maps.Map(document.getElementById("mapa"), options);

    geocoder = new google.maps.Geocoder();

 //propriedades dos makers
    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        clik :true

 });
    //seta no mapa as makers conforme o click do usuario
 map.addListener('click', function(e) {
  marker.setPosition(e.latLng, map);
 });

// //define a posicao do marco no mapa
    marker.setPosition(latlng);

}
 
$(document).ready(function () {
    initializeMap();
	//permite que um endereço digitado pelo usuario sejá marcado o mapa
	function carregarNoMapa(endereco) {
		geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
		
					$('#txtEndereco').val(results[0].formatted_address);
					$('#txtLatitude').val(latitude);
                   	$('#txtLongitude').val(longitude);
		          //gera o maker e o zoom do mapa
					var location = new google.maps.LatLng(latitude, longitude);
					marker.setPosition(location);
					map.setCenter(location);
					map.setZoom(16);
					
				}
			}
		})
	}

//adciona a funcao carregarnomapa ao botao.
	$("#btnEndereco").click(function() {
		if($(this).val() != "")
			carregarNoMapa($("#txtEndereco").val());
	})

	$("#txtEndereco").blur(function() {
		if($(this).val() != "")
			carregarNoMapa($(this).val());
	})
	
	//muda o endereço do campo de busca conforme forem criando marks
	google.maps.event.addListener(map, 'click', function () {
		geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {  
					$('#txtEndereco').val(results[0].formatted_address);
					$('#txtLatitude').val(marker.getPosition().lat());
					$('#txtLongitude').val(marker.getPosition().lng());
				}
			}
		});
	});
	//faz com que ao digitar um endereco o usario receba sujestoes para autocomplete
	$("#txtEndereco").autocomplete({
		source: function (request, response) {
			geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function (results, status) {
				response($.map(results, function (item) {
					return {
						label: item.formatted_address,
						value: item.formatted_address,
						latitude: item.geometry.location.lat(),
          				longitude: item.geometry.location.lng()
					}
				}));
			})
		},
		select: function (event, ui) {
			$("#txtLatitude").val(ui.item.latitude);
    		$("#txtLongitude").val(ui.item.longitude);
			var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
			marker.setPosition(location);
			map.setCenter(location);
			map.setZoom(16);
		}
	});
	//funçao que mostra latitude e longitude na tela ao apertar entre ou botao de enviar
	$("form").submit(function(event) {
		event.preventDefault();
		
		var endereco = $("#txtEndereco").val();
		var latitude = $("#txtLatitude").val();
		var longitude = $("#txtLongitude").val();
		
		alert("Endereço: " + endereco + "\nLatitude: " + latitude + "\nLongitude: " + longitude);
	});

});

